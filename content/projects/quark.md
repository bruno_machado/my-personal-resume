+++
title = 'Quark'
date = 2024-10-11T00:39:01-03:00
draft = false
+++

### The Quark Project

The Quark is a 1:10 scale autonomous vehicle designed as an open-source platform for testing and research in Advanced Driver Assistance Systems (ADAS). Drawing inspiration from the renowned "F1TENTH" project, initially developed by the University of Pennsylvania, Quark aims to provide a fast, low-cost, and flexible solution for training, testing, and evaluating autonomous systems. Over the years, F1TENTH has grown into a global open-source platform, maintained by a diverse community of engineers and researchers, making it widely accessible for various autonomous vehicle applications.

Quark's physical prototype follows the Ackermann steering geometry, replicating real-world driving behaviors at a reduced scale. For localization, the vehicle is equipped with a powerful Jetson Xavier NX processing unit and an array of advanced sensors. These include the "Luxonis OAK D Lite - BMI270" camera, which features an integrated IMU, three "ELP 1MP 120FOV USB" stereo cameras, a "MX218-83 CSI" stereo camera, and encoders attached to a "Maxon EC-4 pole 30mm 24V" motor. With these tools, Quark is already well-equipped for the development of a high-precision localization system, which will soon be enhanced by the addition of a GPS receiver.

To further improve the localization system, a comprehensive solution will be developed through the integration of GNSS (Global Navigation Satellite System) and INS (Inertial Navigation System) technologies, along with wheel speed determination using encoders. This development will be supported by the CARLA (Car Learning to Act) vehicle simulation software and the ROS 2 (Robot Operating System 2) framework, defining the scope of this project.

### My Contributions

Through my Scientif Initiationm, I tend to make several key contributions to this project, particularly in the development of the localization system:

- Sensor Modeling in CARLA: Modeling of sensors, including their inaccuracies, within the CARLA simulation environment.
- Development of a Localization System: Designing and implementing a localization system using a Kalman filter to fuse data from GNSS INS, and encoders.
- ROS 2 Integration: Integrating the localization system using the ROS 2 framework for seamless interaction with other software components.
- Testing and Validation: Conducting testing and validation of the localization system in the CARLA simulation environment.

